package connection;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

import entity.User;

public class UserRepository {
	public static boolean isExist(ConnectionUtil connection, int id) throws SQLException {
		String query = "SELECT * FROM account.user where `id`= ?";
		connection.prepareStatement = connection.connection.prepareStatement(query);
		connection.prepareStatement.setInt(1, id);
		ResultSet rs = connection.prepareStatement.executeQuery();
		return rs.next();
	}

	public static List<Integer> getExistIds(ConnectionUtil connection, String idsStr) throws SQLException {
		System.out.println(idsStr);
		List<Integer> ids = new ArrayList<Integer>();
		String query = "SELECT * FROM account.user where `id` in (?)";
		System.out.println(query);
		connection.prepareStatement = connection.connection.prepareStatement(query);
		connection.prepareStatement.setString(1, idsStr);
		
		ResultSet rs = connection.prepareStatement.executeQuery();
		
		while (rs.next()) {
			System.out.println("in");
			ids.add(rs.getInt("id"));
		}
		return ids;
	}

	public static boolean insert(ConnectionUtil connection, User user) throws SQLException {

		int gender = user.gender == "Female" ? 0 : 1;
		int status = user.status == "Inactive" ? 0 : 1;
		String sql = "INSERT INTO `account`.`user`(`id`,`name`,`gender`,`email`,`status`,`created_at`, `updated_at`) VALUES (?, ?, ?,?,?, ?,?)";

		connection.prepareStatement = connection.connection.prepareStatement(sql);

		connection.prepareStatement.setInt(1, user.id);
		connection.prepareStatement.setString(2, user.name);
		connection.prepareStatement.setInt(3, gender);
		connection.prepareStatement.setString(4, user.email);
		connection.prepareStatement.setInt(5, status);
		connection.prepareStatement.setString(6, user.created_at);
		connection.prepareStatement.setString(7, user.updated_at);

		int isSuccess = connection.prepareStatement.executeUpdate();
		System.out.println("isSuccess: " + isSuccess);
		return isSuccess > 0;
	}

	public static boolean inserts(ConnectionUtil connection, List<User> users) throws SQLException {
		String sql = "INSERT INTO `account`.`user`(`id`,`name`,`gender`,`email`,`status`,`created_at`, `updated_at`) VALUES ";
		boolean isFirst = true;
		if (users.size() > 0) {
			for (User user : users) {
				if (isFirst) {
					sql = MessageFormat.format("{0} {1}", sql, "(?, ?, ?,?,?, ?,?)");
					isFirst = false;
				} else
					sql = MessageFormat.format("{0} , {1}", sql, "(?, ?, ?,?,?, ?,?)");

				System.out.println(sql);

			}
			connection.prepareStatement = connection.connection.prepareStatement(sql);
			int index = 0;
			for (User user : users) {
				int gender = user.gender == "Female" ? 0 : 1;
				int status = user.status == "Inactive" ? 0 : 1;
				connection.prepareStatement.setInt(index * 7 + 1, user.id);
				connection.prepareStatement.setString(index * 7 + 2, user.name);
				connection.prepareStatement.setInt(index * 7 + 3, gender);
				connection.prepareStatement.setString(index * 7 + 4, user.email);
				connection.prepareStatement.setInt(index * 7 + 5, status);
				connection.prepareStatement.setString(index * 7 + 6, user.created_at);
				connection.prepareStatement.setString(index * 7 + 7, user.updated_at);
				index++;
			}
		}
		System.out.println(sql);
		int isSuccess = connection.prepareStatement.executeUpdate();
		System.out.println("isSuccess: " + isSuccess);
		return isSuccess > 0;
	}
}
