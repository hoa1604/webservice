package connection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class ConnectionUtil {

	private final String HOST_NAME = "localhost";
	private final String DB_NAME = "account";
	private final String USER_NAME = "hoant";
	private final String PASSWORD = "@hoa12345";

	public Connection connection;
	public Statement statement;
	public PreparedStatement prepareStatement;

	public ConnectionUtil() throws ClassNotFoundException, SQLException {
		setConnectionString(HOST_NAME, DB_NAME, USER_NAME, PASSWORD);
		this.prepareStatement = null;
		this.statement = connection.createStatement();
	}

	public void setConnectionString(String hostName, String dbName, String userName, String password)
			throws ClassNotFoundException {
		String connectionURL = "jdbc:mysql://" + hostName + ":3306/" + dbName + "?autoReconnect=true&useSSL=false";
		System.out.println(connectionURL);
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			this.connection = DriverManager.getConnection(connectionURL, userName, password);
		} catch (SQLException e) {
			System.out.println("Connect database fail");
			e.printStackTrace();
		}

	}

	public void closeConnectionString() {
		if (this.connection != null)
			try {
				this.connection.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	}

	public ResultSet excuteStatement(String sqlQuery) throws SQLException {
		return this.statement.executeQuery(sqlQuery);
	}

	public int excutePrepareStatement() throws SQLException {
		return this.prepareStatement.executeUpdate();
	}

	public int nt() throws SQLException {
		return this.prepareStatement.executeUpdate();
	}
}
