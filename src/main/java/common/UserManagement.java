package common;

import java.sql.SQLException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

import connection.ConnectionUtil;
import connection.UserRepository;
import entity.User;
import entity.UserResponse;
import entity.UsersResponse;
import webservice.GetRequest;

public class UserManagement {
	public static void GetUsers(ConnectionUtil connection) throws SQLException {
		UsersResponse response = GetRequest.getUsers();
		if (response.code == 200) {
			User[] users = response.convertDataToUsers();
			if (users.length > 0) {
				// get list id insert to check exist
				List<Integer> idInserts = new ArrayList<Integer>();
				String idsStr = null;
				boolean isFirst = true;
				for (User user : users) {
					idInserts.add(user.id);
					if (!isFirst)
						idsStr = MessageFormat.format("{0}, {1}", idsStr, user.id);
					else {
						idsStr = String.valueOf(user.id);
						isFirst=false;
					}
					
				}
				List<Integer> idExisted = UserRepository.getExistIds(connection, idsStr);
				System.out.println(idExisted.toString());
				System.out.println("idInserts "+idInserts.toString());
				idInserts.removeAll(idExisted);
				System.out.println(idInserts.toString());

				// inserts
				List<User> usersInsert = new ArrayList<User>();
				if (idInserts.size() > 0) {
					for (User user : users) {
						if (idInserts.contains(user.id))
							usersInsert.add(user);
					}
					boolean isInserted = UserRepository.inserts(connection, usersInsert);
					if (isInserted)
						System.out.println("Insert success");
					else
						System.out.println("Insert fail");
				}

			}
		} else {
			System.out.println("status code fail");
		}

	}

	public static void GetUserById(ConnectionUtil connection, int id) throws SQLException {
		UserResponse response = GetRequest.getUserById(id);
		if (response.code == 200) {
			User user = response.data;
			System.out.println("Response user: " + user.toString());
			boolean isExist = UserRepository.isExist(connection, id);
			if (isExist)
				System.out.println("User existed in database");
			else {
				boolean isInserted = UserRepository.insert(connection, user);
				if (isInserted)
					System.out.println("Insert success");
				else
					System.out.println("Insert fail");
			}
		} else if (response.code == 404) {
			System.out.println(response.data.message);
		}

	}
}
