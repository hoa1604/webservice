package common;

import java.text.MessageFormat;
import java.util.Scanner;

public class ScannerInput {
	public static String inputRequireString(Scanner scanner, String fieldName) {
		String input = null;
		do {
			System.out.print(MessageFormat.format("Input {0}:", fieldName));
			input = scanner.nextLine();
		} while (input.isEmpty());
		return input;
	}

	public static Integer inputRequireInt(Scanner scanner, String fieldName) {
		Integer number = -1;
		do {
			System.out.print(MessageFormat.format("Input {0}:", fieldName));
			String input = scanner.nextLine();
			if (!input.isEmpty() && input.matches("[0-9]*")) {
				number = Integer.parseInt(input);
			} else {
				System.out.println("Id is a number greater than 0");
			}
		} while (number<0);
		return number;
	}
	
	public static Integer inputIntInRange(int from, int to, Scanner scanner) {
		Integer number = -1;
		do {
			System.out.print("Feature: ");
			String input = scanner.nextLine();
			if (!input.isEmpty() && input.matches(MessageFormat.format("[{0}-{1}]*", from, to))) {
				number = Integer.parseInt(input);
			} else {
				System.out.println(MessageFormat.format("Feature from {0}-{1}. Try again", from, to));
			}

		} while (number < from || number > to);
		return number;
	}
}
