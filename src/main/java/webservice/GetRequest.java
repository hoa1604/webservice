package webservice;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import entity.*;

public class GetRequest {
	static RestTemplate restTemplate = new RestTemplate();

	final static String ROOT_URI = "https://gorest.co.in/public-api/users";

	public GetRequest() {
		ObjectMapper lax = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		MappingJackson2HttpMessageConverter mapping = new MappingJackson2HttpMessageConverter();
		mapping.setObjectMapper(lax);
		List<HttpMessageConverter<?>> list = new ArrayList<HttpMessageConverter<?>>();
		list.add(mapping);
		restTemplate.setMessageConverters(list);
	}

	public static UsersResponse getUsers() {
		ResponseEntity<UsersResponse> response = restTemplate.getForEntity(ROOT_URI, UsersResponse.class);
		return response.getBody();
	}

	public static UserResponse getUserById(int id) {
		String uri = MessageFormat.format("{0}/{1}", ROOT_URI, id);
		ResponseEntity<UserResponse> response = restTemplate.getForEntity(uri, UserResponse.class);
		return response.getBody();
	}
}
