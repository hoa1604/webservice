package entity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class UsersResponse {
	public UsersResponse() {

	}

	public UsersResponse(int code, Meta meta, User[] users) {
		this.code = code;
		this.meta = meta;
		this.data = users;
	}

	public int code;
	public Meta meta;
	public User[] data;

	public User[] convertDataToUsers() {
		return this.data;
	}

}
