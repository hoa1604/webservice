package entity;

import java.util.ArrayList;
public class UserResponse {
	public UserResponse() {

	}

	public UserResponse(int code, Meta meta, User user) {
		this.code = code;
		this.meta = meta;
		this.data = user;
	}

	public int code;
	public Meta meta;
	public User data;
	@Override
	public String toString() {
		return "ID: "+ this.data.id +"; Name: "+ this.data.name+"; email: "+this.data.email;
	}
	
	
}
