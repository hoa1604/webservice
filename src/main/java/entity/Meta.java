package entity;

public class Meta {
	public Meta() {
		
	}
	
	public Meta(Pagination pagination) {
		this.pagination = pagination;
		
	}
	
	public Pagination pagination;

	@Override
	public String toString() {
		return "[pagination=" + pagination.toString() + "]";
	}
}

