package entity;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class User {

	public User() {

	}

	public User(int id, String name, String gender, String email, String status, String created, String updated, String message) {
		this.id = id;
		this.name = name;
		this.gender = gender;
		this.email = email;
		this.status = status;
		this.created_at = created;
		this.updated_at = updated;
		this.message= message;
	}

	public int id;
	public String name;
	public String gender;
	public String email;
	public String status;
	public String created_at;
	public String updated_at;
	public String message;


	@Override
	public String toString() {
		return "ID:" + this.id + " Name: " + this.name;
	}

	public static List<User> convertResultSetToUsers(ResultSet resultSet) {
		List<User> users = new ArrayList<User>();
		boolean hasValue = false;
		try {
			while (resultSet.next()) {
				hasValue = true;
				int id = resultSet.getInt("id");
				String name = resultSet.getString("name");
				String gender = resultSet.getInt("gender") == 0 ? "Female" : "Male";
				String address = resultSet.getString("email");
				String isActive = resultSet.getInt("status") == 0 ? "Inactive" : "Active";
				String created = resultSet.getString("created_at");
				String updated = resultSet.getString("updated_at");
				User user = new User(id, name, gender, address, isActive, created, updated, null);
				users.add(user);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (hasValue)
			return users;
		return null;
	}
}
