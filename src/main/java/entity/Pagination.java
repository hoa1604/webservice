package entity;

public class Pagination {
	public Pagination() {

	}

	public Pagination(int total, int pages, int page, int limit) {
		this.total = total;
		this.pages = pages;
		this.page = page;
		this.limit = limit;

	}

	@Override
	public String toString() {
		return "Pagination [total=" + total + ", pages=" + pages + ", page=" + page + ", limit=" + limit + "]";
	}

	public int total;
	public int pages;
	public int page;
	public int limit;
}
