package main;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import common.ScannerInput;
import common.UserManagement;
import webservice.*;
import entity.*;
import connection.*;

public class Main {

	public static void main(String[] args) {
		try {
			Scanner _scanner = new Scanner(System.in);
			ConnectionUtil connection = new ConnectionUtil();
			mainFunction(_scanner, connection);
			connection.closeConnectionString();
			_scanner.close();
		} catch (Exception ex) {
			System.out.println(ex.toString());
		}
	}

	public static void mainFunction(Scanner scanner, ConnectionUtil connection) throws SQLException {
		System.out.println(
				"\n1. Retrieve all user  \n2. Retrieve 1 user  \n3. Create user \n4. Update user \n5. Delete user \n6. View user \n7. Exit \n");
		Integer _featureNumber = ScannerInput.inputIntInRange(1, 7, scanner);
		switch (_featureNumber) {
		case 1:
			UserManagement.GetUsers(connection);
			mainFunction(scanner, connection);
			break;
		case 2:
			int id = ScannerInput.inputRequireInt(scanner, "User id");
			UserManagement.GetUserById(connection, id);
			break;
		case 7:
			System.out.println("Do you want to exit? \n 1.Yes \n 2.No");
			Integer _number = ScannerInput.inputIntInRange(1, 2, scanner);
			if (_number == 1) {
				System.exit(1);
			} else {
				mainFunction(scanner, connection);
			}
		}
	}

}
